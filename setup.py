#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='oprop',
      package_dir={"":"src"},
      packages = find_packages("src"),
      version='0.1',
      description='optical propagation',
      author='Donald Gavel',
      author_email='donald.gavel@gmail.com',
      keywords = [],
      classifiers = [],
      python_requires = '>=3.8',
      install_requires = ['numpy',
                          'scipy',
                          'astropy',
                          'matplotlib',
                         ]
     )
