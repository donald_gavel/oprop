'''Text coloration.

Example:
--------
from astrobot.tcolor import tcolor
print('{tcolor.fail}This text will be red {tcolor.end}')
print('{tcolor.green}This text will be green {tcolor.end}')

See:
----
https://stackoverflow.com/questions/287871/how-to-print-colored-text-to-the-terminal

'''
class tcolor:
    header = '\033[95m'
    blue = '\033[94m'
    cyan = '\033[96m'
    green = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    end = '\033[0m'
    bold = '\033[1m'
    underline = '\033[4m'
